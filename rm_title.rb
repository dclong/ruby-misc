#!/usr/bin/ruby
# This program helps to remove files end with tilde.
# It is not very useful because you can "find" and "rm" in shell to achieve the same purpose. 
def find_tilde(dir)
    # get entries 
    sub = Dir.entries dir
    sub.delete '.'
    sub.delete '..'
    tilde = []
    sub.each do |x|
        x = File.join dir, x
        if File.file? x
            if x.end_with? '~'
                tilde.push x
            end
            next
        end
        tilde.concat find_tilde(x)
    end
    return tilde
end

class String
    def to_bool
        return true if self == true || self =~ (/(true|t|yes|y|1)$/i)
        return false if self == false || self =~ (/(false|f|no|n|0)$/i)
        raise ArgumentError.new("invalid value for Boolean: \"#{self}\"")
    end
end

# Remove files end with tilde.
# The files removed are returned.
def remove_tilde(dir, verify)
    tilde = find_tilde dir
    tilde.each do |x|
        puts x
    end
    puts "The above are all found files ending with tilde.\n\n"
    removed = []
    tilde.each do |x|
        puts x
        if verify
            puts "Do you want to remove the above file?\n"\
                 "Y/y: Yes.\n"\
                 "N/n: No.\n"\
                 "A/a: Yes to all.\n"\
                 "C/c: Cancel.\n"
            choice = gets
            if choice == 'y'
                File.delete x
                removed.push x
                puts "File deleted.\n\n"
                next
            end
            if choice == 'n'
                next
            end
            if choice == 'a'
                next
                verify = false
            end
            puts "Operation canceled.\n\n"
            next
        else
            File.delete x
            removed.push x
            puts "File deleted.\n\n"
            next
        end
    end
    return removed
end

if __FILE__ == $0
    if ARGV.length == 1
        remove_tilde ARGV[0], true
        exit
    end
    if ARGV.length == 2
        remove_tilde ARGV[0], ARGV[1].to_bool
        exit
    end
end



