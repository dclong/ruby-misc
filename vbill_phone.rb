#!/usr/bin/env ruby
# This version extract phone numbers from the statement and send billing information via message.
# This is not suitable currently because a message has a very limited length. 
def vbill(statement, sendemail=false, page=3)
    require 'pdf-reader'
    reader = PDF::Reader.new(statement)
    # read the third page
    # text = reader.page(page).text
    # read pages
    text = ""
    for i in 1..5
        text += reader.page(i).text
    end
    # puts text
    # match for due date
    regex = Regexp.new(/(\d{2}\/\d{2}\/\d{2})/)
    mdata = regex.match(text)
    if mdata.nil?
        unsupported_format
        return 
    end
    due_date = mdata[1]
    # match for total current charges
    regex = Regexp.new(/\nTotal Current Charges\$(\d{1,3}\.\d{2})\n/)
    # regex = Regexp.new(/Total Current Charges.*\$(\d{1,3}\.\d{2})/)
    mdata = regex.match(text)
    if mdata.nil?
        # puts "here!"
        unsupported_format
        return
    end
    bill_total = mdata[1]
    # match for account charges
    regex = Regexp.new(/Account Charges \& Credits\$(\d{1,3}\.\d{2})\n/)
    mdata = regex.match(text)
    if mdata.nil?
        unsupported_format
        return
    end
    account_charge = mdata[1].to_f 
    # split text by into lines
    text = text.split("\n")
    # match for personal charges
    regex = Regexp.new(/(\d{3}-\d{3}-\d{4}\$\d{1,3}\.\d{2})pg/)
    mdatas = text.map{|x| regex.match(x)}
    mdatas.delete_if{|x| x==nil}
    if mdatas.length == 0
        unsupported_format
        return
    end
    people_charge = mdatas.map{|x| x[1].split("$")}
    cal_bill due_date, bill_total, account_charge, people_charge, sendemail
end
def cal_bill(due_date, bill_total, account_charge, people_charge, sendemail=false)
    # calculate final bill for each person
    avg_account_charge = account_charge * 100.0 / people_charge.length()
    avg_account_charge = avg_account_charge.ceil / 100.0
    # construct second part of the email body
    email_body2 = "\nAnd the following is a summary of the statement in case you are interested.\n\n"\
    "Total bill: $" + "%.2f" % bill_total + "\n"\
    "Account charge: $" + "%.2f" % account_charge + "\n"
    people_charge.each{
        |x|
        email_body2 += x[0].to_s + " charge: $" + "%.2f" % x[1] + "\n"
    }
    email_body2 += "\nIf you have any question about the bill, please let me know ASAP, "\
    "otherwise please remember to pay your part of bill before " + due_date + ".\n\nThanks!\n\n"
    #puts "Average account charge is: ", avg_account_charge
    final_bills = people_charge 
    # people_charge will also be mutated, do not use it any more
    for i in 0...final_bills.length
        x = final_bills[i][1].to_f
        # avoid double saveing issue ... a better way is to use integers ... 
        # I think a better way is to use currency class to do this. 
        final_bills[i][1] = (x + avg_account_charge).round(2)
    end
    # format en email content that you want to send to people
    email_subject = "Verizon Bill Due " + due_date
    # construct first part of the email body
    email_body1 = "Hello, everyone!\n\n"\
    "Our Verizon bill has arrived. The following is the specific bill for each of you.\n\n"
    to_pay = 0
    final_bills.each{
        |x|
        email_body1 += x[0] + ": $" + "%.2f" % x[1] + "\n"
        to_pay += x[1]
    }
    to_pay = to_pay.round 2
    email_body1 += "Total to pay VS Total bill: $" + "%.2f" % to_pay + " VS $" + "%.2f" % bill_total + ".\n" 
    email_body = email_body1 + email_body2
    print email_body
    if sendemail.class == String
        sendemail = sendemail.to_bool
    end
    if sendemail
        # extract phone numbers 
        phones = "" 
        final_bills.each{
            |x|
            phones += x[0].gsub("-","") + "@vtext.com, "
        }
        email_bills email_subject, email_body, phones
    end
end
def email_bills(subject, body, emails)
    require 'action_mailer'
#    ActionMailer::Base.default :from => 'Chuanlong Du <duchuanlong@gmail.com>'
    ActionMailer::Base.raise_delivery_errors = true
    ActionMailer::Base.delivery_method = :smtp
    member_emails = emails
    # read in password for 'firedragon.du@gmail.com'
    password = read_password
    ActionMailer::Base.smtp_settings = {
        :address   => "smtp.gmail.com",
        :port      => 587,
        :domain    => "gmail.com",
        :authentication => :plain,
        :user_name      => "firedragon.du@gmail.com",
        :password       => password,
        :enable_starttls_auto => true
    }
    mail = ActionMailer::Base.mail(:to=>member_emails)
    mail.from = "firedragon.du@gmail.com"
    mail.reply_to = "duchuanlong@gmail.com"
    mail.subject = subject
    mail.body = body
    mail.deliver
    message = "Bills has been sent to the following email addresses.\n"
    member_emails = member_emails.split(",")
    member_emails.each do |x| 
        message += x + "\n"
    end
    puts message
end
def unsupported_format()
    puts "Bill information not found.\n"\
         "Make sure that you have specified the right page containing breakdown of charges.\n"
end
def read_password()
    require 'highline/import'
    return ask('Please enter the password for "firedragon.du@gmail.com":' + "\n"){
        |q|
        q.echo = "*"
    }
end
class String
    def to_bool
        return true if self == true || self =~ (/(true|t|yes|y|1)$/i)
        return false if self == false || self =~ (/(false|f|no|n|0)$/i)
        raise ArgumentError.new("invalid value for Boolean: \"#{self}\"")
    end
end
if __FILE__ == $0
    if ARGV.length == 1
        vbill ARGV[0]
        exit
    end
    if ARGV.length == 2
        vbill ARGV[0], ARGV[1]
        exit
    end
    if ARGV.length == 3
        vbill ARGV[0], ARGV[1], ARGV[2]
    end
    if ARGV.length > 3
        puts 'Too many arguments!'
        exit
    end
    puts 'Path of the statement file is required!'
end



